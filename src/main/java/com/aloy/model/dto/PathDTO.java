package com.aloy.model.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
public class PathDTO {

    private Integer port;
    private String domain;
    private String contextPath;
    
    public static synchronized PathDTO create() {
        return new PathDTO();
    }
    public Integer getPort() {
        return port;
    }
    public void setPort(Integer port) {
        this.port = port;
    }

    public String getDomain() {
        return domain;
    }
    public void setDomain(String domain) {
        this.domain = domain;
    }
    public PathDTO withDomain(String domain) {
        this.domain = domain;
        return this;
    }
    public String getContextPath() {
        return contextPath;
    }
    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }
    public PathDTO withContextPath(String contextPath) {
        this.contextPath = contextPath;
        return this;
    }
    public PathDTO withPort(Integer port) {
        this.port = port;
        return this;
    }
	@Override
	public String toString() {
		return "http://" + domain + ":" + port + contextPath + "/";
	}
    
}
