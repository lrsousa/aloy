package com.aloy.model.dto;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("path.server")
public class ServerConfigurationDTO {
    
    @Value("#{'${path.server.pathPorts}'.split(',')}")
    private Integer[] pathPorts;
    @Value("#{'${path.server.redundancePorts}'.split(',')}")
    private Integer[] redundancePorts;
    
    private String domain;
    
    private String contextPath;
    
    public Integer[] getPathPorts() {
        return pathPorts;
    }
    public void setPathPorts(Integer[] pathPorts) {
        this.pathPorts = pathPorts;
    }
    public Integer[] getRedundancePorts() {
        return redundancePorts;
    }
    public void setRedundancePorts(Integer[] redundancePorts) {
        this.redundancePorts = redundancePorts;
    }
    public String getDomain() {
        return domain;
    }
    public void setDomain(String domain) {
        this.domain = domain;
    }
    public String getContextPath() {
        return contextPath;
    }
    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }
}
