package com.aloy.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aloy.model.dto.PathDTO;
import com.aloy.service.PathService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("path")
@Api(value = "path_controller", tags="path")
public class PathController {

    @Autowired
    PathService pathService;
    
    @GetMapping("/find/{email:.+}")
    @ApiOperation(value = "find one")
    public ResponseEntity<String> find(@ApiParam(name = "email", value = "email", required = true)
                                        @PathVariable(value = "email", required = true) String email) {
        return ResponseEntity.status(HttpStatus.FOUND)
                             .body(this.pathService.getPath(email));
    
    }
}
