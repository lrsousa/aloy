package com.aloy;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class AloyApplication  {

    public static void main(String[] args) {
        SpringApplication.run(AloyApplication.class, args);
    }

}
