package com.aloy.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aloy.model.dto.PathDTO;
import com.aloy.model.dto.ServerConfigurationDTO;

@Service
public class PathService {

    private final ConnectionService  connectionService;
    private final ServerConfigurationDTO serverDTO;

    @Autowired
    public PathService(ConnectionService connectionService, ServerConfigurationDTO serverDTO) {
        this.connectionService = connectionService;
        this.serverDTO = serverDTO;
    }

    public String getPath(String email) {
        return this.connectionService
                   .meh(PathDTO.create()
                               .withDomain(serverDTO.getDomain())
                               .withPort(this.getPort(email))
                               .withContextPath(serverDTO.getContextPath()))
                   .toString();
    }
    
    private Integer getPort(String email) {
        if(email.toUpperCase().matches("^[A-H].*")) {
            return this.serverDTO.getPathPorts()[0];
        } else if(email.toUpperCase().matches("^[J-R].*")) {
            return this.serverDTO.getPathPorts()[1];
        } else if(email.toUpperCase().matches("^[S-Z].*")) {
            return this.serverDTO.getPathPorts()[2];
        }
        throw new RuntimeException("Invalid Email!");
    }



}
