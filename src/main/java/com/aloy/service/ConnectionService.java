package com.aloy.service;

import com.aloy.model.dto.PathDTO;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

@Service
public class ConnectionService {
    
    public PathDTO meh(PathDTO meh) throws RestClientException {
        RestTemplate restTemplate = new RestTemplate();
        try {
            restTemplate.getForObject(meh.toString().concat("health"), String.class);
            return meh;
        } catch (RestClientException e) {
            meh.withPort(meh.getPort() + 10);
            restTemplate.getForObject(meh.toString().concat("health"), String.class);
            return meh;
        }
    }
}
